#!/bin/bash
set -e

if [[ "$1" == nginx* ]]; then
 	
 	echo "Nginx command called"
 	printenv 
 	service php5-fpm restart
 	nginx -g 'daemon off;'
fi

exec "$@"