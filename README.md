# README #

[Emoncms](https://emoncms.org/) for ARMv7 arch.


Tested on Raspberry Pi 2 

![emoncms_graphic.png](https://bitbucket.org/repo/6kgopL/images/119855345-emoncms_graphic.png)


### Deploy manually ###

```shell
# To (re)create the container after rebuilding the image or modifying the yml config. file
$>docker-compose up -d
```

### TODO ###

* Update entrypoint script to automatically create the database and user account if not exist (cf. emoncms install procedure online)
* [Internationalization](https://github.com/emoncms/emoncms/blob/master/docs/gettext.md#4-install-gettext)