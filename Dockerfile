FROM armv7/armhf-ubuntu

MAINTAINER Antoine SAUVAGE "antowan@wawax.co"

ENV DEBIAN_FRONTEND=noninteractive

ARG timezone=Asia/Tokyo

################################################
#             Install dependencies             #
################################################
RUN apt-get update --fix-missing && apt-get upgrade -y

RUN apt-get install -y \
	nginx \
	mysql-client \
	php5 \
	php5-fpm \
	php5-cgi \
	php5-mysql \
	php5-curl \
	php-pear \
	php5-dev \
	php5-mcrypt \
	php5-json \
	git-core \
	redis-server build-essential \
	ufw \
	ntp \
	nano

# Update the timezone
RUN echo $timezone > /etc/timezone && \
	dpkg-reconfigure -f noninteractive tzdata


RUN pear channel-discover pear.swiftmailer.org && \
	pecl install channel://pecl.php.net/dio-0.0.6 redis swift/swift

################################################
#               Configure apache               #
################################################

RUN \
	sh -c 'echo "extension=dio.so" > /etc/php5/cli/conf.d/20-dio.ini' && \
	sh -c 'echo "extension=redis.so" > /etc/php5/cli/conf.d/20-redis.ini'

#RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf
COPY config/emoncms/emoncms.nginx.conf /etc/nginx/sites-available/default

################################################
#                Install emoncms               #
################################################

WORKDIR /var/www/

RUN mkdir -p emoncms
RUN chown www-data:www-data emoncms
RUN git clone https://github.com/emoncms/emoncms.git emoncms

#Create data repositories for emoncms feed engine's
RUN mkdir /var/lib/phpfiwa /var/lib/phpfina /var/lib/phptimeseries
RUN chown www-data:root /var/lib/phpfiwa /var/lib/phpfina /var/lib/phptimeseries

# Update SQL settings [TODO] with sed and use global variable from mysql container
WORKDIR /var/www/emoncms
COPY config/emoncms/settings.php settings.php

# Clone modules
WORKDIR /var/www/emoncms/Modules
RUN git clone https://github.com/emoncms/dashboard.git
RUN git clone https://github.com/emoncms/app.git

################################################
#           Expose service and data            #
################################################

EXPOSE 80
VOLUME ["/var/www/"]

################################################
#               Run the service                #
################################################
WORKDIR /var/www/emoncms/

COPY entrypoint.sh /entrypoint.sh
#RUN chmod +x /entrypoint.sh

# Put start check and sql create script if db doesn t exist
ENTRYPOINT ["/entrypoint.sh"]

# Run nginx in foreground (otherwise the container exit)
CMD ["nginx"]